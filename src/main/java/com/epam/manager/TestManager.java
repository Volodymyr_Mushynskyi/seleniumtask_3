package com.epam.manager;

import com.epam.pages.EmailPage;
import com.epam.pages.GmailLoginPage;
import com.epam.utils.MyProperties;

public class TestManager {

    private MyProperties myProperties;

    public TestManager(){
        myProperties = new MyProperties();
    }

    public TestManager openGmailAndLogin() {
        GmailLoginPage gmailLoginPage = new GmailLoginPage();
        gmailLoginPage.fillEmailInputField(myProperties.getPropertyValue("email"));
        gmailLoginPage.clickByEmailNextButton();
        gmailLoginPage.fillPasswordInputField(myProperties.getPropertyValue("password"));
        gmailLoginPage.clickByPasswordNextButton();
        return new TestManager();
    }

    public TestManager clickByComposeButton() {
        EmailPage emailPage = new EmailPage();
        emailPage.clickByComposeButton();

        return new TestManager();
    }

    public TestManager fillEmailFields() {
        EmailPage emailPage = new EmailPage();
        emailPage.fillRecipientEmailInputField();
        emailPage.fillSubjectNameInputField();
        emailPage.fillTextAreaInputField();
        return new TestManager();
    }

    public TestManager sendMessageFromDraftFolder() {
        EmailPage emailPage = new EmailPage();
        emailPage.clickByDraftMessage();
        emailPage.clickBySendButton();
        return new TestManager();
    }

    public TestManager closeNewMessageWindow() {
        EmailPage emailPage = new EmailPage();
        emailPage.clickByCloseButton();
        return new TestManager();
    }

    public TestManager verifyThatMessageSavedAsDraft() {
       EmailPage emailPage = new EmailPage();
       emailPage.verifyIsMessageInDraftFolder();
       return new TestManager();
   }
}
